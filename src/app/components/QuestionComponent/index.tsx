import { initialState, MainState } from "app/models/mainState"
import * as React from "react"
import * as style from "./style.scss"

export class QuestionComponent extends React.Component<{}, MainState> {
  state = initialState

  answer = (isCorrect: boolean) => {
    this.setState(state => ({
      ...state,
      questionNumber: state.questionNumber + 1
    }))
  }

  render() {
    const state = this.state
    return (
      <section className={style.main}>
        <div>Вопрос {state.questionNumber}/{state.questionCount}</div>
        <div>Фото {state.question.photoId}</div>
        <div>Эмоция {state.question.emotionId}</div>
        <button onClick={() => this.answer(true)}>Да</button>
        <button onClick={() => this.answer(false)}>Нет</button>
      </section>
    )
  }
}
