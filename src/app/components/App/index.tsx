import { QuestionComponent } from "app/components/QuestionComponent/index"
import * as React from "react"
import * as style from "./style.scss"

export class App extends React.Component {

  render() {
    return (
      <div className={style.main}>
        <QuestionComponent/>
      </div>
    )
  }
}

