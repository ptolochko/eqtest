import { App } from "app/components/App"
import * as React from "react"
import { hot } from "react-hot-loader"

export const RootApp = hot(module)(App)
