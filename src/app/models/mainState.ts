import { Question } from "app/models/question"

export const initialState: MainState = {
  correctCount: 0,
  errorCount: 0,
  question: {
    emotionId: 1,
    photoId: 1
  },
  questionCount: 100,
  questionNumber: 1
}

export interface MainState {
  question: Question
  questionNumber: number
  questionCount: number
  correctCount: number
  errorCount: number
}
