import { RootApp } from "app/index"
import * as React from "react"
import * as ReactDOM from "react-dom"

interface Settings {
  htmlElement: HTMLElement
}

export function create(settings: Settings) {
  ReactDOM.render(
    <RootApp/>,
    settings.htmlElement
  )

}
